package main

import (
	"io/ioutil"
	"log"
	"os"
	"strings"
)

func main() {

	file := os.Getenv("ES_FILE")
	log.Println("Reading ", file)

	in, _ := ioutil.ReadFile(file)

	ls := strings.Split(string(in), "\n")

	out, _ := os.Create("test.json")
	defer out.Close()

	for _, l := range ls {
		if len(l) == 0 {
			continue
		}
		out.WriteString("{\"index\": {\"_type\": \"log\"}}\n")
		out.WriteString(l + "\n")
	}

	log.Println("Done")

}
