docker run \
-d \
-v $(pwd):/app \
-e ES_FILE=test.json \
-e ES_INDEX=test4 \
-e ES_PORT=8084 \
--net=host --name=loader11 --restart=always es_bulk

docker run \
-d \
-v $(pwd):/app \
-e ES_FILE=test.json \
-e ES_INDEX=test5 \
-e ES_PORT=8085 \
--net=host --name=loader12 --restart=always es_bulk
